from math import pi, sqrt
import numpy as np
# Based on ase.dft.dos.

class DOS:
  def __init__(self, w_k, e_skn,  width=0.1, window=None, npts=201):
    """Electronic Density Of States object.

    width: float
      Width of guassian smearing.
    window: tuple of two float
      Use ``window=(emin, emax)``.  If not specified, a window
      big enough to hold all the eigenvalues will be used.
    npts: int
      Number of points.

    """

    self.npts = npts
    self.width = width
    self.w_k = w_k
    self.e_skn = e_skn

    if window is None:
      emin = self.e_skn.min() - 5 * self.width
      emax = self.e_skn.max() + 5 * self.width
    else:
      emin, emax = window

    self.energies = np.linspace(emin, emax, npts)

  def delta(self, energy):
    """Return a delta-function centered at 'energy'."""
    x = -((self.energies - energy) / self.width)**2
    return np.exp(x) / (sqrt(pi) * self.width)

  def get_energies(self):
    """"return energy mesh."""
    return self.energies

  def get_dos(self, psi_skn_w=None):
    """Get array of DOS values."""

    dos_list = []
    for isp, e_skn in enumerate(self.e_skn):
      dos = np.zeros(self.npts)
      for k, w in enumerate(self.w_k):
        for n, e in enumerate(e_skn[k]):
          if n>0 and e<e_skn[k][n-1]:
            break
          res = w * self.delta(e)
          if psi_skn_w is None:
            dos += res
          else:
            dos += res*psi_skn_w[isp,k,n]
      dos_list.append(dos)
    return np.array(dos_list)

def get_all_psi_skn_w(e_skn, psi_sksn, bnd_ne):
  '''
  Reduce psi_sksn to psi_skn_w by summing over symmetry operation indiex.
  '''
  psi_skn_w = np.zeros(list(e_skn.shape)+[psi_sksn.shape[-1]], dtype=float)
  for isp in range(e_skn.shape[0]):
    for k in range(e_skn.shape[1]):
      for isy in range(psi_sksn.shape[2]):
        for n in range(bnd_ne[k,1],bnd_ne[k,2]):
          for a in range(psi_sksn.shape[-1]):
            psi_skn_w[isp,k,n,a] += (psi_sksn[isp,k,isy,n-bnd_ne[k,1],a] \
                            *np.conj(psi_sksn[isp,k,isy,n-bnd_ne[k,1],a])).real
  return psi_skn_w/psi_sksn.shape[2]

def get_dos(filename, U=None, tp=None, save=False):
  '''
  get dos at U and tp
  '''
  import h5py
  import time
  import sys
  Ryd_eV = 1.0#13.605698066

  #U_lower_bound = 0.0
  #U_upper_bound = 12.0
  #U_step = 0.5
  # begin loop over U
  #U_list = np.arange(U_lower_bound, U_upper_bound, U_step)
  #for U in U_list:
  #U = 5.1

  if U == None:
      print "Please input first argument U."
      sys.exit()

  if tp == None:
      print "Please input second argument tp"
      sys.exit()
  else:
      tp_list = [tp]

  for tp in tp_list:
    f = h5py.File(filename, 'r')
    # Fermi level
    e_fermi = f["/E_FERMI"][0]*Ryd_eV
    #print 'e_fermi=', e_fermi
    # Band energy to eV
    e_skn = f["/BND_EK"][...]*Ryd_eV
    #print 'e_skn=',e_skn
    # k-ponits weight
    w_k = f["/KPT_WT"][...]
    #print 'w_k=', w_k
    # Band indices for contructing the local projector.
    # bnd_ne[ik,0]: total number of bands
    # bnd_ne[ik,1:2]: bottom/top bands used to contruct the local projector
    bnd_ne = f["/BND_NE"][...]
    # expansion coefficients <\psi | f>, (spin, k-points, sym_ops, bands, orbitals), band index shifted by ne(:,1)
    psi_sksn = f["/BND_VK"][...]
    f.close()
  
    psi_skn_w = get_all_psi_skn_w(e_skn, psi_sksn, bnd_ne)
    nbmax_plot = np.min(bnd_ne[:,2])
    # Energy window
    #emin = np.min(e_skn[:,:,8]); emax = np.min(e_skn[:,:,nbmax_plot])
    emin = np.min(e_skn[:,:,0]); emax = np.max(e_skn[:,:,nbmax_plot-1])
    window = (emin, emax)
    dos = DOS(w_k, e_skn,  width=0.05, window=window, npts=1001)
    energies = dos.get_energies()
    dos_t = dos.get_dos()
    #psi_sksn_f = np.zeros(e_skn.shape, dtype=float)
    #for a in [0,2]:#range(0,nbmax_plot,2):
    #  psi_sksn_f += psi_skn_w[:,:,:,a]
    #dos_f = dos.get_dos(psi_sksn_f)
  
    # store metadata for the plot.
    filename = "dosU"+str(U)+".h5"
    f = h5py.File("dos.h5", 'w')
    f["/energies"] = energies
    f["/dos_tot"] = dos_t[0]
    #f["/dos_f"] = dos_f[0]
    f.close()
  
    import matplotlib.pyplot as plt
    filename = "DOSU"+str(U)+"tp"+str(tp)+".png"
    #plt.ion()
    plt.figure(0)
    plt.fill_between(energies - e_fermi, 0, dos_t[0], facecolor = 'grey', alpha = 0.5, label = 'tot')
    plt.plot(energies - e_fermi, dos_t[0], color = 'grey', label = 'tot')
    #plt.plot(energies - e_fermi, dos_f[0], color = 'red', label = '$f$')
    #plt.axvline(x = e_fermi, ls = '--')
    plt.axvline(x = 0.0, ls = '--')
    plt.title("Density of States U="+str(U)+" tp="+str(tp))
    plt.xlabel("E (eV)")
    plt.ylabel("DOS (states/f.u.)")
    plt.xlim(-6.5,6.5)
    plt.ylim(0,4)
    plt.legend(loc='upper right')
    if save == True:
        plt.savefig(filename)
    plt.show()
#    time.sleep(0.0)
    plt.clf()
